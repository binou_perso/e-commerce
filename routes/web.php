<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 Route::get('/', 'HomeController@index')->name('welcome');
 Route::Post('/reservation', 'ReservationController@reserv')->name('reservation.reserv');
 Route::Post('/contact', 'ContactController@message')->name('contact.message');

Auth::routes();

//Route de l'administrateur
Route::group(['prefix'=>'admin', 'middleware'=>'auth', 'namespace'=>'admin'], function (){
 //    route du dashboard
    Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
//    route des slides
    Route::resource('slider', 'SliderController');
//    route des categories
    Route::resource('category', 'CategoryController');
//    route des articles
    Route::resource('article', 'ArticleController');
 //    route des réservations
    Route::get('reservation', 'ReservationController@index')->name('reservation.index');
    
    Route::post('reservation/{id}', 'ReservationController@statut')->name('reservation.statut');

    
    Route::delete('reservation/{id}', 'ReservationController@destroy')->name('reservation.destroy');
    
    Route::get('contact', 'ContactController@index')->name('contact.index');
    
    Route::get('contact/{id}', 'ContactController@show')->name('contact.show');
    
     Route::delete('contact/{id}', 'ContactController@destroy')->name('contact.destroy');

   
});
