<?php

namespace App\Http\Controllers;

use App\Message;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ContactController extends Controller
{
 

   
 public function message(Request $request) {
  
  $this->validate($request,[
	     'name' => 'required',
	     'email' => 'required|email', 
           'subject' => 'required',
	     'message' => 'required', 
	 ]);
  
  $message = new Message();
	$message->name = $request->name;
	$message->email = $request->email;
	$message->subject = $request->subject;
	$message->message = $request->message;
	$message->save();
	Toastr::success('Message envoyé avec succès', 'Success', ["positionClass" => "toast-top-center"]);
	
	return redirect()->back();

 }
}
