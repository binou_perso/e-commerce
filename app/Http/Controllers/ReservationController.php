<?php

namespace App\Http\Controllers;


use App\Reservation;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ReservationController extends Controller
{
 public function reserv(Request $request){
  
   $this->validate($request,[
	     'name' => 'required', 
	     'phone' => 'required', 
	     'email' => 'required|email', 
	     'dateandtime' => 'required', 
	 ]);
   
   $reservation = new Reservation();
	$reservation->name = $request->name;
	$reservation->phone = $request->phone;
	$reservation->email = $request->email;
	$reservation->date_and_time = $request->dateandtime;
	$reservation->message = $request->message;
	$reservation->statut = false;
	$reservation->save();
	Toastr::success('Reservation fait avec succès', 'Success', ["positionClass" => "toast-top-center"]);
	
	return redirect()->back();
 }
	
}
