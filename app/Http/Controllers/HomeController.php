<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Articles;
use App\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $sliders = Slider::all();
      $articles = Articles::all();
      $categories = Category::all();
	  return view('welcome', compact('sliders', 'articles', 'categories'));  
      
    }
}
