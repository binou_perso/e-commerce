<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notifications\ReservationConfirmed;
use App\Reservation;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Notification;
use function redirect;
use function view;

class ReservationController extends Controller
{
 public function index() {
  
  $reservations = Reservation::all();
   return view('admin.reservation.index', compact('reservations'));
  
 }
  
 public function statut($id) {
  
  $reservation = Reservation::find($id);
  $reservation->statut = true;
  $reservation->save();
  Notification::route('mail',$reservation->email)
            ->notify(new ReservationConfirmed());
  Toastr::success('Reservation confirmée avec succès', 'Title', ["positionClass" => "toast-top-center"]);
  return redirect()->back();
  
 }

 
 public function destroy($id) {
  
   Reservation::find($id)->delete();
   Toastr::success('Reservation supprimée avec succès', 'Title', ["positionClass" => "toast-top-center"]);
   return redirect()->back();
 }

}
