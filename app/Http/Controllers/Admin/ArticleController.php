<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Articles;
use App\Category;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Articles::all();
	  return view('admin.article.index', compact('articles'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         
	  
	  $categories = Category::all();
        return view('admin.article.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         request()->validate([
	     'category' => 'required', 
	     'name' => 'required|string', 
	     'description' => 'string', 
	     'price' => 'required|integer', 
	     'image' => 'mimes:jpeg,jpg,bmp,png', 
	 ]);
	   
	 $image = $request->file('image');
	 $slug = str_slug($request->name);
	 if (isset($image)){
	    $currenDate = Carbon::now()->toDateString();
	    $imagename = $slug .'-'. $currenDate .'-'. uniqid() .'.'. $image->getClientOriginalExtension();
	    if (!file_exists('uploads/article')){
	     mkdir('uploads/article', 0777, true);
	    }
	    $image->move('uploads/article', $imagename);
	 }
	else {
        $imagename = 'default.png';
      }
	
	$article = new Articles();
	$article->category_id = $request->category;
	$article->name = $request->name;
	$article->description = $request->description;
	$article->price = $request->price;
	$article->image = $imagename;
      $article->save();
	
	
	return redirect()->route('article.index')->with('success', 'Article ajouté avec succès !');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Articles::find($id);
	  return view('admin.article.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       request()->validate([
	    'category' => 'required', 
	     'name' => 'string', 
	     'description' => 'string', 
	     'price' => 'integer', 
	     'image' => 'mimes:jpeg,jpg,bmp,png', 
	 ]);
	 $image = $request->file('image');
	 $slug = str_slug($request->name);
	 $article = Articles::find($id);
	 if (isset($image)){
	    $currenDate = Carbon::now()->toDateString();
	    $imagename = $slug .'-'. $currenDate .'-'. uniqid() .'.'. $image->getClientOriginalExtension();
	    if (!file_exists('uploads/article')){
	     mkdir('uploads/article', 0777, true);
	    }
	    $image->move('uploads/article', $imagename);
	 }
	else {
        $imagename = $article->image;
      }
	
             $article->name = $request->name;
             $article->description = $request->description;
             $article->price = $request->price;
             $article->image = $imagename;  
             $article->save();
	return redirect()->route('article.index')->with('success', 'Article modifié avec succès !');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $article = Articles::find($id);
	 if (file_exists('uploads/article/'.$article->image))
	 {
	    unlink('uploads/article/'.$article->image);
	   
	 }
	  $article->delete();  
	  return redirect()->back()->with('success', 'Article supprimé avec succès !');
    }
}
