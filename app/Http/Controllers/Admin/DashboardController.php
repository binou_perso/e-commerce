<?php

namespace App\Http\Controllers\Admin;

use App\Articles;
use App\Category;
use App\Http\Controllers\Controller;
use App\Reservation;
use App\Slider;
use App\Message;
use function view;

class DashboardController extends Controller 
{
     public function index()
     {
	
	$categorycount = Category::count();
	$articlecount = Articles::count();
	$slidercount = Slider::count();
	$reservations = Reservation::where('statut', false)->get();
	$contactCount = Message::count();
	
         return view('admin.dashboard', compact('categorycount', 'articlecount', 'slidercount', 'reservations', 'contactCount')); 
     }
}
