<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{

 protected $table = 'articles';
 
     protected $fillable = [
         'name', 'description', 'price', 'image', 'category_id',
    ];
     
//     relation avec categorie
    public function category() {
     
//     appel du model des articles
      return $this->belongsTo(Category::class);
     
    }
}
