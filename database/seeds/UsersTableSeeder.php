<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' =>  bcrypt('password'),
            
            
        ]);
       User::create([
            'name' => 'Commerce',
            'email' => 'commerce@gmail.com',
            'password' =>  bcrypt('password'),
            
            
        ]);
        User::create([
           'name' => 'Blog',
            'email' => 'blog@gmail.com',
            'password' =>  bcrypt('password'),
            
            
        ]);
        User::create([
           'name' => 'Galoppin',
            'email' => 'alain@gmail.com',
            'password' => bcrypt('password'),
            
        ]);
    }
}
