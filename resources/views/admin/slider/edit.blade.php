@extends('layouts.app')

<!--titre du slider-->
@section('title', 'Slider')

@push('css')


@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
		 <!--		Message flash d"erreur de validation-->
		   @include('layouts.partial.messages')
<!--	Message flash-->
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Editer le slide</h4>
                </div>
                <div class="card-body">
                  <div class="card-content">
			 <form method="POST" action="{{ route('slider.update', $slider->id)}}" enctype="multipart/form-data">
			      @csrf
				@method('PUT')
				 <div class="row">
					<div class="col-md-12">
					  <div class="form-group">
					    <label class="bmd-label-floating">Titre</label>
					    <input type="text" name="title" class="form-control" value="{{ $slider->title}}">
					  </div>
					</div>
				 </div>
				 <div class="row">
				     <div class="col-md-12">
					 <div class="form-group">
					   <label class="bmd-label-floating">Sub titre</label>
					   <input type="text" name="sub_title" class="form-control" value="{{ $slider->sub_title}}">
					 </div>
				     </div>
                         </div>
				 <div class="row">
				     <div class="col-md-12">
					 
					   <label class="control-label">Image du slide</label>
					   <input type="file" name="image" class="form-control">
					 
				     </div>
                         </div><br><br>
				 <a href="{{ route('slider.index')}}" class="btn btn-danger">Retour</a>
				<button type="submit" class="btn btn-primary">Editer</button>
			  
			 </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
       

@endsection


@push('script')

 

@endpush
