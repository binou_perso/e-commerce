@extends('layouts.app')

<!--titre du slider-->
@section('title', 'Slider')

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
<!--		Message flash-->
            @include('layouts.partial.messages')
<!--	end	Message flash-->
<!--		 boutton d'ajout du nouveau slide-->
		  <a href="{{ route('slider.create') }}" class="btn btn-info">Ajouter un slide</a> 
<!--	end	 boutton d'ajout du nouveau slide-->		  
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Liste des slides</h4>
			
                </div>
                <div class="card-body">
                  <div class="table-responsive">
			 <table id="example" class="table" style="width:900px;">
                      <thead class=" text-primary">
				  <th>ID</th>
				  <th>Titre </th>
				  <th>Sub titre</th>
				  <th>Image</th>
				  <th>Date de création</th>
				  <th>Date de modification</th>
				  <th>Actions</th>
                      </thead>
                      <tbody>
			       @foreach ($sliders as $key=>$slider)
				    <tr>
					  <td class="badge">{{ $key +1 }}</td>
					  <td>{{ $slider->title }}</td>
					  <td>{{ $slider->sub_title }}</td>
					  <td>{{ $slider->image }}</td>
					  <td>{{ $slider->created_at }}</td>
					  <td>{{ $slider->updated_at }}</td>
					  <td>
					   <a href="{{ route('slider.edit', $slider->id )}}" class="btn btn-info btn-sm">Editer</a>
					   <form id="delete-form-{{ $slider->id}}" action="{{ route('slider.destroy',$slider->id) }}" method="POST" style="display: none;">
					     @csrf
					     @method('DELETE')
					   </form>
					   <button type="button" class="btn btn-info btn-sm"  onclick="if(confirm('Voulez-vous vraiment supprimer ce slide ?')){
						 event.preventDefault();
						 document.getElementById('delete-form-{{ $slider->id}}').submit();
						}else{
						 event.preventDefault();
						}" >Supprimer</button>
					  </td>
				    </tr>
				  @endforeach 
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
       

@endsection


@push('script')

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
 <script>
        $(document).ready(function() {
        $('#example').DataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            }

        } );
         $("[data-toggle=tooltip]").tooltip();
           } );
</script>
 

@endpush
