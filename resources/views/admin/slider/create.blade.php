@extends('layouts.app')

<!--titre du slider-->
@section('title', 'Slider')

@push('css')


@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
		 <!--		Message flash d"erreur de validation-->
		  
		   @if ($errors->any())
                    <div class="alert alert-danger">
			   <ul>
			       @foreach ($errors->all() as $error)
				   <div class="alert alert-danger">
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<i class="material-icons">*</i>
				    </button>
				    <span>
					<b> Danger - {{ $error }}</span>
				  </div>
				 @endforeach
			   </ul>
                       
                    </div>
                @endif
		  
<!--	Message flash-->
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Ajouter un nouveau slide</h4>
                </div>
                <div class="card-body">
                  <div class="card-content">
			 <form method="POST" action="{{ route('slider.store')}}" enctype="multipart/form-data">
			      @csrf
				 <div class="row">
					<div class="col-md-12">
					  <div class="form-group">
					    <label class="bmd-label-floating">Titre</label>
					    <input type="text" name="title" class="form-control">
					  </div>
					</div>
				 </div>
				 <div class="row">
				     <div class="col-md-12">
					 <div class="form-group">
					   <label class="bmd-label-floating">Sub titre</label>
					   <input type="text" name="sub_title" class="form-control">
					 </div>
				     </div>
                         </div>
				 <div class="row">
				     <div class="col-md-12">
					 
					   <label class="control-label">Image du slide</label>
					   <input type="file" name="image" class="form-control">
					 
				     </div>
                         </div><br><br>
				 <a href="{{ route('slider.index')}}" class="btn btn-danger">Retour</a>
				<button type="submit" class="btn btn-primary">Ajouter le slide</button>
			  
			 </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
       

@endsection


@push('script')

 

@endpush
