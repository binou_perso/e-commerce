@extends('layouts.app')

<!--titre du slider-->
@section('title', 'Reservation')

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
<!--		Message flash-->
            @include('layouts.partial.messages')
<!--	end	Message flash-->		  
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Listes des reservation</h4>
			
                </div>
                <div class="card-body">
                  <div class="table-responsive">
			 <table id="example" class="table" style="width:900px;">

                      <thead class=" text-primary">
				  <th>ID</th>
				  <th>Nom</th>
				  <th>Contact</th>
				   <th>Email</th>
				  <th>Date et heure</th>
				  <th>Message</th>
				  <th>Statut</th>
				  <th>Actions</th>
                      </thead>
                      <tbody>
			       @foreach ($reservations as $key=>$reservation)
				    <tr>
					  <td class="badge">{{ $key +1 }}</td>
					  <td>{{ $reservation->name}}</td>
					  <td>{{ $reservation->phone }}</td>
					  <td>{{ $reservation->email }}</td>
					  <td>{{ $reservation->date_and_time }}</td>
					  <td>{{ $reservation->message }}</td>
					  <td>
					      @if($reservation->statut == true) 
						    <span class="btn btn-info">Confirmé</span>
						@else
						    <span class="btn btn-danger">Pas Confirmé</span>
						@endif
					  </td>
					  <td>
					     @if($reservation->statut == false)
						 
					   <form id="status-form-{{ $reservation->id}}" action="{{ route('reservation.statut', $reservation->id)}}" method="POST" style="display: none;">
					     @csrf
					   </form>
					   <button type="button" class="btn btn-info btn-sm"  onclick="if(confirm('Avez vous vérifié cette réservation par appel ?')){
						 event.preventDefault();
						 document.getElementById('status-form-{{ $reservation->id}}').submit();
						}else{
						 event.preventDefault();
						}" ><i class="material-icons">done</i></button>
						@endif
					   <form id="delete-form-{{ $reservation->id}}" action="{{ route('reservation.destroy', $reservation->id)}}" method="POST" style="display: none;">

					     @csrf
					     @method('DELETE')
					   </form>
					   <button type="button" class="btn btn-info btn-sm"  onclick="if(confirm('Voulez-vous vraiment supprimer cette reservation ?')){
						 event.preventDefault();
						 document.getElementById('delete-form-{{ $reservation->id}}').submit();
						}else{
						 event.preventDefault();
						}" ><i class="material-icons">delete</i></button>
					  </td>
				    </tr>
				  
				  @endforeach 
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
       

@endsection


@push('script')

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
{!! Toastr::message() !!} 
<script>
        $(document).ready(function() {
        $('#example').DataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            }

        } );
         $("[data-toggle=tooltip]").tooltip();
           } );
</script>
 

@endpush
