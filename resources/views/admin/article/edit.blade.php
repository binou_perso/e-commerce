@extends('layouts.app')

<!--titre du slider-->
@section('title', 'Categorie')

@push('css')


@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
		 <!--		Message flash d"erreur de validation-->
		   @include('layouts.partial.messages')
<!--	Message flash-->
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Editer la categorie</h4>
                </div>
                <div class="card-body">
                  <div class="card-content">
			 <form method="POST" action="{{ route('article.update', $article->id)}}">
			      @csrf
				@method('PUT')
				 <div class="row">
					<div class="col-md-12">
					  <div class="form-group">
					    <label class="bmd-label-floating">Categorie</label>
					    <input type="text" name="name" class="form-control" value="{{ $article->name}}">
					  </div>
					</div>
				 </div>
				 <div class="row">
					<div class="col-md-12">
					  <div class="form-group">
					    <label class="bmd-label-floating">Nom</label>
					    <input type="text" name="name" class="form-control" value="{{ $article->name}}">
					  </div>
					</div>
				 </div>
				 <div class="row">
				     <div class="col-md-12">
					 <div class="form-group">
					   <label class="bmd-label-floating">Description</label>
					   <input type="text" name="slug" class="form-control" value="{{ $article->slug}}">
					 </div>
				     </div>
                         </div><br><br>
				 <a href="{{ route('category.index')}}" class="btn btn-danger">Retour</a>
				<button type="submit" class="btn btn-primary">Editer</button>
			  
			 </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
       

@endsection


@push('script')

 

@endpush
