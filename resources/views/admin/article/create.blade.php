@extends('layouts.app')

<!--titre du slider-->
@section('title', 'Article')

@push('css')


@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
		 <!--		Message flash d"erreur de validation-->
		  
		   @if ($errors->any())
                    <div class="alert alert-danger">
			   <ul>
			       @foreach ($errors->all() as $error)
				   <div class="alert alert-danger">
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<i class="material-icons">*</i>
				    </button>
				    <span>
					<b> Danger - {{ $error }}</span>
				  </div>
				 @endforeach
			   </ul>
                       
                    </div>
                @endif
		  
<!--	Message flash-->
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Ajouter un article</h4>
                </div>
                <div class="card-body">
                  <div class="card-content">
			 <form method="POST" action="{{ route('article.store')}}" enctype="multipart/form-data">
			      @csrf
				 <div class="row">
					<div class="col-md-12">
					  <div class="form-group">
					    <label class="bmd-label-floating">Choisir une categorie</label>
					    <select  name="category" class="form-control">
					         @foreach ($categories as $key=>$category)
						   <option value="{{ $category->id }}">{{ $category->name }}</option>
						   @endforeach
					     
					    </select>
					  </div>
					</div>
				 </div>
				 <div class="row">
					<div class="col-md-12">
					  <div class="form-group">
					    <label class="bmd-label-floating">Nom</label>
					    <input type="text" name="name" class="form-control">
					  </div>
					</div>
				 </div>
				 <div class="row">
					<div class="col-md-12">
					  <div class="form-group">
					    <label class="bmd-label-floating">Description</label>
					    <textarea name="description" id="" cols="30" rows="10" name="description" class="form-control"></textarea>
					  </div>
					</div>
				 </div>
				 <div class="row">
				     <div class="col-md-12">
					 <div class="form-group">
					   <label class="bmd-label-floating">Prix</label>
					   <input type="number" name="price" class="form-control">
					 </div>
				     </div>
                         </div>
				 <div class="row">
				     <div class="col-md-12">
					   <label class="control-label">Image</label>
					   <input type="file" name="image" class="form-control">
				     </div>
                         </div><br><br>
				  <a href="{{ route('article.index')}}" class="btn btn-danger">Retour</a>
				<button type="submit" class="btn btn-primary">Ajouter</button>
			 </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
       

@endsection


@push('script')

 

@endpush
