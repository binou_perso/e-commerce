@extends('layouts.app')

<!--titre du slider-->
@section('title', 'Contact')

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">	  
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">{{ $message->subject }}</h4>
                </div>
                <div class="card-body">
			<div class="card-content">
			     <div class="row">
				    <div class="col-md-12">
				     <strong style="width:700px;">Name: {{ $message->name}}</strong> <br>
				     <b>Email: {{ $message->email }}</b> <br>
				     
				     <strong>Message: {{ $message->name}}</strong> <br>
				     
				     <p>{{ $message->message }}</p>
				    </div>
				<a href="{{ route('contact.index') }}" class="btn btn-danger">Retour</a>
				<div class="clearfix"></div>
                          </div>
                   </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
       

@endsection


@push('script')



@endpush
