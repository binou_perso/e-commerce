@extends('layouts.app')

<!--titre du slider-->
@section('title', 'Contact')

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
<!--		Message flash-->
            @include('layouts.partial.messages')
<!--	end	Message flash-->		  
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Listes des messages</h4>
			
                </div>
                <div class="card-body">
                  <div class="card-content table-responsive">
			 <table id="example" class="table" style="width:900px;">
                      <thead class=" text-primary">
				  <th>ID</th>
				  <th>Nom</th>
				   <th>Email</th>
				    <th>Subjection</th>
				  <th>Message</th>
				  <th>Actions</th>
                      </thead>
                      <tbody>
			       @foreach ($messages as $key=>$message)
				    <tr>
					  <td class="badge">{{ $key +1 }}</td>
					  <td>{{ $message->name}}</td>
					  <td>{{ $message->email }}</td>
					  <td>{{ $message->subject }}</td>
					  <td>{{ $message->message }}</td>
					  <td>
					   <a href="{{ route('contact.show', $message->id)}}" class="btn btn-info btn-sm"><i class="material-icons">details</i></a>
					   <form id="delete-form-{{ $message->id}}" action="{{ route('contact.destroy', $message->id)}}" method="POST" style="display: none;">
					     @csrf
					     @method('DELETE')
					   </form>
					   <button type="button" class="btn btn-info btn-sm"  onclick="if(confirm('Voulez-vous vraiment supprimer ce message ?')){
						 event.preventDefault();
						 document.getElementById('delete-form-{{ $message->id}}').submit();
						}else{
						 event.preventDefault();
						}" ><i class="material-icons">delete</i></button>
					  </td>
				    </tr>
				  
				  @endforeach 
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
       

@endsection


@push('script')

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script language="JavaScript" src="https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
 <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
	  @if ($errors->any())
		@foreach ($errors->all() as $error)
		    <script>
			toastr.error('{{ $error }}');
		    </script>
		@endforeach
       @endif
{!! Toastr::message() !!}
<script>
        $(document).ready(function() {
        $('#example').DataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            }

        } );
         $("[data-toggle=tooltip]").tooltip();
           } );
</script>
 

@endpush
