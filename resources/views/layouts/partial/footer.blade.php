<footer class="footer">
	    <div class="container-fluid">
		
		<div class="copyright float-right">
		  &copy;
		  {{ date('Y') }}, Binou pro <i class="material-icons">favorite</i> by bINOU aBOULAYE
           
		</div>
	    </div>
	  </footer>