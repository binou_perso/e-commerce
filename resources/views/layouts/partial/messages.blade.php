@if ($errors->any())
                    <div class="alert alert-danger">
			   <ul>
			       @foreach ($errors->all() as $error)
				   <div class="alert alert-danger">
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<i class="material-icons">*</i>
				    </button>
				    <span>
					<b> Danger - {{ $error }}</span>
				  </div>
				 @endforeach
			   </ul>
                       
                    </div>
@endif


		  @if (session('success'))      
		     <div class="alert alert-success">
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<i class="material-icons">*</i>
				    </button>
				    <span>
					<b> Danger - {{ session('success') }}</span>
			</div>
                @endif

